import collections

from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS

from ingestion import ArticleSet


class ArticleClusterer:

    def __init__(self, corpus: ArticleSet):
        self.corpus = corpus
        self.result_by_group = collections.defaultdict(list)
        self.result_by_pmid = {doc.pmid: None for doc in corpus.article_set()}

    def run(self):
        vectorizer = TfidfVectorizer(
            input='content', encoding='utf-8', lowercase=True, stop_words='english', strip_accents='unicode',
            token_pattern=r'[a-zA-Z][\w\d]+', ngram_range=(1,3),
        )

        processed = vectorizer.fit_transform([c.text() for c in self.corpus.article_set()]).toarray()

        sc = AgglomerativeClustering(
            affinity='cosine', linkage='average', distance_threshold=0.98, n_clusters=None, compute_full_tree=True
        ).fit(processed)

        for index, doc in enumerate(self.corpus.article_set()):
            cluster_id = sc.labels_[index]
            self.result_by_group[cluster_id].append(doc)
            self.result_by_pmid[doc.pmid] = cluster_id



def generate_labels_for_clusters(clusterer: ArticleClusterer) -> dict:
    cluster_labels = {}
    for cluster_id, doc_list in clusterer.result_by_group.items():
        possible_names = collections.Counter()
        for doc in doc_list:
            tokens = doc.text().lower().split(' ')
            for first, second, third in zip(tokens, tokens[1:], tokens[2:]):
                if first in ENGLISH_STOP_WORDS:
                    continue
                possible_names[first] += 1
                possible_names['{} {}'.format(first, second)] += 1
                possible_names['{} {} {}'.format(first, second, third)] += 1

        top_50 = possible_names.most_common(50)
        selected_name = sorted(top_50, key=lambda item: len(item[0]) * item[1], reverse=True)[0]
        name_tokens = selected_name[0].split(' ')
        cluster_labels[cluster_id] = ' '.join([name_token.capitalize() for name_token in name_tokens])
    return cluster_labels
