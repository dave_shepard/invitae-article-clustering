import codecs
import os
import urllib.request

import bs4


def ingest_pmid_file(filename:str) -> list:
    pmids = []
    with codecs.open(filename, encoding='utf-8') as in_file:
        for line in in_file:
            pmids.append(line.strip())
    return pmids


class ArticleSet:

    def __init__(self, pmids: list, base_url:str, cache_dir:str):
        self.pmids = set(pmids)
        self.base_url = base_url
        self.cache_dir = cache_dir

        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)

        self.__missing = []

        for pmid in self.pmids:
            if not self.__article_cached(pmid):
                self.__download_article(pmid)

        for missing_id in self.__missing:
            self.pmids.remove(missing_id)


    def missing(self) -> list:
        if self.__missing:
            return self.__missing

    def article_set(self):
        for pmid in self.pmids:
            yield ArticleFile(pmid, os.path.join(self.cache_dir, '{}.txt'.format(pmid)))

    def __article_cached(self, pmid) -> bool:
        return os.path.exists(os.path.join(self.cache_dir, '{pmid}.txt'.format(pmid=pmid)))

    def __download_article(self, pmid) -> bool:
        with urllib.request.urlopen(self.base_url.format(pmid=pmid)) as req:
            body = req.read()
            if body:
                try:
                    abstract = bs4.BeautifulSoup(body, features='html.parser').find_all('div', class_='abstr')[
                        0].find_all('div')
                except IndexError:
                    self.__missing.append(pmid)
                    return False
            else:
                self.__missing.append(pmid)
                return False
            out_filename = '{}.txt'.format(pmid)
            with codecs.open(os.path.join(self.cache_dir, out_filename), 'w', encoding='utf-8') as out_file:
                out_file.write(abstract[0].get_text())
            return True


class Article:

    def __init__(self, name):
        raise NotImplementedError('This class is meant to be abstract')

    def text(self):
        """
        Provide the text of the instance

        :return:
        """
        raise NotImplementedError('This class is meant to be abstract')

    def pmid(self):
        """
        Return PMID of the file

        :return: str
        :return:
        """
        raise NotImplementedError('This class is meant to be abstract')


class ArticleFile(Article):

    def __init__(self, pmid, filename:str):
        self._pmid = pmid
        self.filename = filename

    def text(self):
        with codecs.open(self.filename) as in_file:
            return in_file.read()

    def pmid(self):
        return self._pmid

