import argparse

from clustering import ArticleClusterer, generate_labels_for_clusters
from exporting import export_labels
from ingestion import ingest_pmid_file, ArticleSet


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Cluster articles')
    parser.add_argument('-i', '--in_file', dest='in_file', action='store', help="Filename with list of PMIDs")
    parser.add_argument('-u', '--pubmed-url', dest='pubmed_url', action='store', help='Base URL for PubMed',
                        default='https://www.ncbi.nlm.nih.gov/pubmed/?term=%5B{pmid}%5D')
    parser.add_argument('-c', '--cache-dir', dest='cache_dir', action='store', help='Location of article cache',
                        default='./article-cache/')
    parser.add_argument('-o', '--output', dest='output_filename', action='store', help='Filename to write classifications')
    args = parser.parse_args()

    pmids = ingest_pmid_file(args.in_file)
    print('Acquiring articles ...')
    article_set = ArticleSet(pmids, args.pubmed_url, args.cache_dir)

    if article_set.missing:
        missing = article_set.missing()
        pluralize = 's'
        if len(missing) == 1:
            pluralize = ''
        print('PMID{pluralize} {pmids} not found'.format(pluralize=pluralize, pmids=', '.join(article_set.missing())))

    print('Clustering ...')

    clusterer = ArticleClusterer(article_set)
    clusterer.run()

    print('Clusters generated')

    label_names = generate_labels_for_clusters(clusterer)
    export_labels(clusterer, label_names, args.output_filename)

