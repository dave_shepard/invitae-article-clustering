import codecs

from clustering import ArticleClusterer


def export_labels(clusterer: ArticleClusterer, cluster_names: dict, output_filename: str):
    with codecs.open(output_filename, 'w', encoding='utf-8') as out_file:
        for cluster_id, doc_list in clusterer.result_by_group.items():
            cluster_name = cluster_names[cluster_id]
            for doc in doc_list:
                out_file.write('{pmid}\t{cluster_name}\n'.format(pmid=doc.pmid(), cluster_name=cluster_name))
