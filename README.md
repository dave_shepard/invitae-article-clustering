# Article clustering

This tool clusters downloads articles from PubMed and clusters them based on what conditions they describe,
 based on shared keywords. The main script is `cluster_articles.py`, which both downloads articles and
 outputs a file in the following format:
 
 
 . | . 
 --- | ---
 PMID | Cluster ID
 
Columns are separated by tabs.


To use it, first create a virtual environment using Python 3. Then, install dependencies using `pip -r requirements.txt`.

To run, use:

`python cluster_articles.py -i {PMID file} -o {results file}`

You can specify additional options for the cache directory and PubMed base using the `-c` and `-u` options, respectively.
For more information, enter `python cluster_articles.py -h`.


# Approach
First, the package performs these processing steps:

* All words are lowercased

* Punctuation is removed

* Any word that does not match the following pattern is removed:

	* The first character is a letter
	* The following characters may be letters or numbers. This allows acronyms like “DM1,” but excludes numbers like “29th.”
	
* Stopwords from a list of common English stop words, provided by scikit-learn, are removed.

* n-grams up to 3 words in length were preserved, to consider disease names as a single word

The program uses TF-IDF to find significant keywords in each article. Results from TF-IDF are clustered using 
agglomerative clustering with cosine similarity. The linkage type is average linkage, which minimizes the average of 
distances between all pairs of clusters, and the distance threshold is 0.98. This threshold was developed through 
experimentation. 

It uses agglomerative clustering for two reasons:

 * The user does not specify the initial number of clusters, as with algorithms like K-means. 
   Going in, the user does not have to know how many different topics there are in articles in the data set.
 * Agglomerative clustering works well with uneven cluster sizes, which was the case with both the gold set 
   and the test set. 
 


# Design

This program works in three different phases: importing, clustering, and exporting data. Each of these
is in its own module so that it's easy to get a high-level overview of the program from the entry point, `cluster_articles.py`.

`ArticleSet` represents a list of files in memory. It handles the process of downloading and caching files
from PubMed. It also handles errors such as articles with empty abstracts. 

The `ArticleFile` class represents a file with an article text in it. It implements an interface called
`Article` with a view toward making the code extensible. If the program were to support multiple
sources types, such as text loaded from strings or a database, then classes could be implemented by implementing the
the `Article` interface. The `Article` interface contains a `pmid` member and a `text` method that
returns the entire contents of a file into memory to allow for streaming the content. If each abstract 
were large, it might make sense to read it line by line rather than loading it all into memory at once, 
but article abstracts are, by definition, short.

To avoid loading too much data into memory at once, `ArticleFile.text()` loads and closes the file each time it is called.
This makes disk I/O a constraint of the program, but is helpful for larger data sets.

`ArticleClusterer` performs both TF-IDF and clustering because in this program, they are essentially a single task. 
Following the design of other ML libraries such as scikit-learn, the class both performs the work and contains the 
results. The user builds an `ArticleClusterer` object with an `ArticleSet`,
then calls the `run()` method. Results can either be looked up by cluster ID, or by PMID. Lookup-by-PMID is available
because this is the easiest format for displaying the results in the desired output format. Lookup-by-cluster is available
because this feature makes certain tasks in ML more convenient.

Results are generated in `exporting.export_labels()`. While it might have been possible to create a method on `ArticleClusterer`
to do this, to follow the single-responsibility principle and support the possibility of extending the class further, 
this method is a separate module-level function.

Finally, cluster names are guessed using a simple algorithm that looks for the longest, most-frequently-occurring 3-gram in
`clustering.generate_labels_for_clusters()`.


# Performance

## Complexity

* TF-IDF is a function of the number of tokens in a corpus and the number of documents in that corpus. 
  Every token in each document must be counted. Then, the number of documents with an instance of each word
  must be counted. Then, each document’s term count must be divided by 1 + log(the number of documents with 
  that term). Therefore, the algorithmic complexity of TF-IDF in general is O(nL log nL) where n is the total
  number of documents and L is the average length.
* Agglomerative clustering has a time complexity of O(n<sup>3</sup>) and requires O(n<sup>2</sup>) memory. Better algorithms 
  do exist that can improve the performance of agglomerative clustering in certain cases. In theory, this should be very slow for large
  data sets, but in this instance, the size was not large enough to matter.

Therefore, the overall complexity of this approach, worst case, is O(n<sup>3</sup>).

## Accuracy

| Metric | Score |
---------|---------
| Normalized Mutual Information (NMI) | 0.98 |
| Homogeneity | 1.0 | 
| Completenesss | 0.96 |
| V-Measure  | 0.98 |

Running the program on the gold set produced 8 clusters. 6 of them were essentially the same categories as the gold set. There 
were two additional communities, each with a single member, community 6 and community 7. #6 contained a single article
on neurofibromatosis (24032991), and #7 had an article on Bardet-Biedel Syndrome. Both of these outliers only mention
the term a single time, and 24032991 uses some very different vocabulary to discuss neurofibromatosis, so it's not 
surprising that they were misclassified. 

Finding the abstracts for 24939608 required manually downloading the article from Wiley, and the abstract for 8001324 
could not be found at all. In this case, the program outputs an error to the user, but continues with clustering.

For the sample dataset provided, the algorithm was highly accurate and perfumed in an acceptable amount of time (1.7 "real" 
seconds according to `time`).  

On the test dataset, spot checking revealed different metrics. The full results can be seen in `pmid_test_labeled.txt`, but 
to summarize, there were eight clusters:

| Cluster ID | Name | Article Count | 
-------------|------|----------------
| 0 | Marfan Syndrome | 28  |
| 1 |  Noonan Syndrome  |  10  |
| 2 |  Turner Syndrome  |  15  |
| 3 |  Syndrome De Marfan |  1  (28383843) |
| 4 |  Cornelia De Lange Syndrome  |  5 
| 5 |  Lynch Syndrome  |  7  |
| 6 |  Lung Cancer  |  9  |
| 7 |  Exercise Recommendations For* |  1  (25911666) |

\* This cluster's name appears to be inconsistent

In general, this approach was effective on this dataset, in that similar diseases were placed together in similar 
categories. It shared with the gold set that there were a few outlier clusters. In some regards, 
it's not surprising that 28383843 showed up in its own cluster, because it was in French. It shared almost no vocabulary 
with any other article in the database, so it made sense for the clustering algorithm to place it in its own cluster. 
25911666, likewise, is on exercise recommendations for Marfan Syndrome patients, and is very short. Similar to the other
outliers, it only uses the disease term a single time, so it seems there is not enough data for the classifier to use.

The cluster naming was mostly accurate with the exception of "triple-negative breast cancer" which was named "breast cancer."
This is probably a function of inconsistent hyphenation in the abstracts. Also, capitalization was not preserved because
the algorithm works with only lowercase letters.


# Limitations
In terms of performance, it’s possible agglomerative clustering would be too slow for a larger 
dataset. To work with a larger set, I would explore the use of a more performant clustering algorithm. I chose agglomerative
clustering because of its accuracy and the requirement that the algorithm handle an unknown number of clusters.

Clearly, this approach produces outliers, so the accuracy could be improved. The number of outliers could probably be
reduced by fine-tuning clustering algorithm parameters. While different settings did improve the performance on the gold
set, the same changes negatively impacted the performance on the test set. For example, if the TF-IDF step did not consider
bigrams, there would be a single outlier in the gold set, but the Marfan and Turner syndrome clusters in the test set
would be merged into a single community. Different changes to the clustering algorithm produced similar results. I chose 
this approach because it seemed more useful to an end-user to have a 
few small outliers than to have a large community with two overlapping topics.

Finally, a better cluster-naming algorithm would look for longer n-grams, and also track the original text of those n-grams
to preserve case. Also, it obviously produced incorrect, but understandable, titles for the single-member clusters.


# Troubleshooting

## error: invalid command 'bdist_wheel'

Some linux distributions are missing the wheel package. Run `pip install wheel`.


